﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiJWTandMVC1.Models
{
    public class UserLogin
    {
        public string Usuario { get; set; }
        public string Password { get; set; }
    }
}