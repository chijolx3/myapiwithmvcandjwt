﻿using ApiJWTandMVC1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace JWTandMVC.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            ViewBag.TipoMaterial = GetMaterial();
            IEnumerable<Producto> producto = null;

            //var cookieContainer = new CookieContainer();
            //using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");

                ////----------------------
                //cookieContainer.Add(new Uri("https://" + Request.Url.Host.ToString()), new Cookie("tecCookie", Request.Cookies["tecCookie"].Value));
                ////----------------------

                //HTTP GET
                var responseTask = client.GetAsync("Productos");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Producto>>();
                    readTask.Wait();

                    producto = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    producto = Enumerable.Empty<Producto>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(producto);
        }

        private Producto GetProductoByID(int id)
        {
            Producto producto = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Productos/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Producto>();
                    readTask.Wait();

                    producto = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    producto = new Producto();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return producto;
        }

        private List<Material> GetMaterial()
        {
            List<Material> material = null;

            //var cookieContainer = new CookieContainer();
            //using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");

                ////----------------------
                //cookieContainer.Add(new Uri("https://" + Request.Url.Host.ToString()), new Cookie("tecCookie", Request.Cookies["tecCookie"].Value));
                ////----------------------

                //HTTP GET
                var responseTask = client.GetAsync("Materials/");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Material>>();
                    readTask.Wait();

                    material = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    material = new List<Material>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return material;
        }

        private Material GetMaterialByID(int id)
        {
            Material material = null;

            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                ////----------------------
                cookieContainer.Add(new Uri("https://" + Request.Url.Host.ToString()), new Cookie("tecCookie", Request.Cookies["tecCookie"].Value));
                ////----------------------

                //HTTP GET
                var responseTask = client.GetAsync("Materials/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Material>();
                    readTask.Wait();

                    material = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    material = new Material();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return material;
        }

        // GET: Products/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.TipoMaterial = GetMaterial();
            return View(GetProductoByID(id));
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.TipoMaterial = GetMaterial();
            return View();
        }

        // POST: Products/Create
        [HttpPost]
        public ActionResult Create(Producto producto)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<Producto>("Productos", producto);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Products/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Producto producto)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Producto>("Productos/" + id, producto);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Products/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Productos/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }
    }
}
