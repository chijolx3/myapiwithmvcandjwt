﻿using ApiJWTandMVC1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace JWTandMVC.Controllers
{
    public class MaterialsController : Controller
    {
        //[Authorize]
        // GET: Materials
        public ActionResult Index()
        {
            IEnumerable<Material> materiales_productos = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Materials");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Material>>();
                    readTask.Wait();

                    materiales_productos = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    materiales_productos = Enumerable.Empty<Material>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(materiales_productos);
        }

        private Material GetMaterialByID(int id)
        {
            Material material = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Materials/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Material>();
                    readTask.Wait();

                    material = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    material = new Material();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return material;
        }

        private List<Material> GetTipoMaterial()
        {
            List<Material> tipo_material = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44338/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Material");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Material>>();
                    readTask.Wait();

                    tipo_material = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    tipo_material = new List<Material>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return tipo_material;
        }

        // GET: Materials/Details/5
        public ActionResult Details(int id)
        {
            //ViewBag.TipoProducto = GetTipoProductoByID(id);
            return View(GetMaterialByID(id));
        }

        // GET: Materials/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Materials/Create
        [HttpPost]
        public ActionResult Create(Material material)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<Material>("Materials", material);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Materials/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Materials/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Material material)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Material>("Materials/" + id, material);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Materials/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Materials/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Material material)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Materials/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }
    }
}
