﻿using ApiJWTandMVC1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace JWTandMVC.Controllers
{
    public class ComprasController : Controller
    {
        // GET: Compras
        public ActionResult Index()
        {
            IEnumerable<Compra> compra = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Compras");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Compra>>();
                    readTask.Wait();

                    compra = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    compra = Enumerable.Empty<Compra>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(compra);
        }

        private Compra GetCompraByID(int id)
        {
            Compra compra = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Compras/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Compra>();
                    readTask.Wait();

                    compra = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    compra = new Compra();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return compra;
        }

        // GET: Compras/Details/5
        public ActionResult Details(int id)
        {
            //ViewBag.TipoProducto = GetTipoProductoByID(id);
            return View(GetCompraByID(id));
        }

        // GET: Compras/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Compras/Create
        [HttpPost]
        public ActionResult Create(Compra compra)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<Compra>("Compras", compra);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Compras/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Compras/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Compra compra)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Compra>("Compras/" + id, compra);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Compras/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Compras/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Compra compra)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Compras/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }
    }
}
