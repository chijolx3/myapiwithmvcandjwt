﻿using ApiJWTandMVC1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace JWTandMVC.Controllers
{
    public class PedidosController : Controller
    {
        private List<Usuario> GetUserData()
        {
            List<Usuario> usuario = null;

            //var cookieContainer = new CookieContainer();
            //using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");

                ////----------------------
                //cookieContainer.Add(new Uri("https://" + Request.Url.Host.ToString()), new Cookie("tecCookie", Request.Cookies["tecCookie"].Value));
                ////----------------------

                //HTTP GET
                var responseTask = client.GetAsync("Usuarios/");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Usuario>>();
                    readTask.Wait();

                    usuario = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    usuario = new List<Usuario>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return usuario;
        }

        // GET: Pedidos
        public ActionResult Index()
        {
            ViewBag.UserData = GetUserData();
            IEnumerable<Pedido> pedido = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pedidos");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<Pedido>>();
                    readTask.Wait();

                    pedido = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    pedido = Enumerable.Empty<Pedido>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(pedido);
        }

        private Pedido GetPedidoByID(int id)
        {
            Pedido pedido = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Pedidos/" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Pedido>();
                    readTask.Wait();

                    pedido = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    pedido = new Pedido();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }

            }
            return pedido;
        }

        // GET: Pedidos/Details/5
        public ActionResult Details(int id)
        {
            ViewBag.UserData = GetUserData();
            return View(GetPedidoByID(id));
        }

        // GET: Pedidos/Create
        public ActionResult Create()
        {
            ViewBag.UserData = GetUserData();
            return View();
        }

        // POST: Pedidos/Create
        [HttpPost]
        public ActionResult Create(Pedido pedido)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<Pedido>("Pedidos", pedido);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Pedidos/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.UserData = GetUserData();
            return View();
        }

        // POST: Pedidos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Pedido pedido)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Pedido>("Pedidos/" + id, pedido);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }

        // GET: Pedidos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Pedidos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Pedido pedido)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44306/api/");
                //HTTP DELETE
                var deleteTask = client.DeleteAsync("Pedidos/" + id.ToString());
                deleteTask.Wait();

                var result = deleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

            }
            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
            return View();
        }
    }
}
